\contentsline {chapter}{\numberline {Acknowledgments\hspace {-96pt}}}{viii}{chapter*.4}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Background}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Motivation: Underwater Domain Challenges}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Objectives}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Outline}{3}{section.1.4}
\contentsline {chapter}{\numberline {2}State Of The Art}{4}{chapter.2}
\contentsline {chapter}{\numberline {3}I-AUV Architecture}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Hardware Overview}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Software Overview}{8}{section.3.2}
\contentsline {chapter}{\numberline {4}MoveIt! Architecture \& Capabilities}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Modelling a new robot}{13}{section.4.1}
\contentsline {section}{\numberline {4.2}Kinematics Solver}{15}{section.4.2}
\contentsline {section}{\numberline {4.3}Planning \& What Is Behind}{15}{section.4.3}
\contentsline {section}{\numberline {4.4}System Controllers}{17}{section.4.4}
\contentsline {section}{\numberline {4.5}Pick Functionality}{19}{section.4.5}
\contentsline {section}{\numberline {4.6}Perception}{20}{section.4.6}
\contentsline {chapter}{\numberline {5}MoveIt! Based Sea-Intervention Application}{21}{chapter.5}
\contentsline {section}{\numberline {5.1}Implemented Software Architecture \& Capabilities}{21}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}I-AUV modeling in MoveIt!}{21}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}I-AUV Kinematics Solver}{25}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}I-AUV Robot State \& Joint State Synchronization}{26}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}I-AUV Controller}{26}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}I-AUV Planning Capabilities}{27}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}Workspace Analysis}{27}{subsection.5.1.6}
\contentsline {section}{\numberline {5.2} "Valve Turning" Benchmark Task}{28}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}\textbf {\emph {Valve Turning Phases:}}}{29}{subsection.5.2.1}
\contentsline {chapter}{\numberline {6}Experimental Setup \& Results}{37}{chapter.6}
\contentsline {section}{\numberline {6.1}Experiments Methodology}{37}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}MoveIt! \& Rviz Simulation}{37}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}UWSim Environment}{37}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}The Real System In The Water Tank}{38}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Results \& Analysis}{39}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}MoveIt! \& Rviz Simulation Results}{39}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Water Tank Experiment Results}{40}{subsection.6.2.2}
\contentsline {chapter}{\numberline {7}Conclusions \& Future Work}{57}{chapter.7}
\contentsline {section}{\numberline {7.1}Conclusions}{57}{section.7.1}
\contentsline {section}{\numberline {7.2}Future Work}{57}{section.7.2}
\contentsline {chapter}{\numberline {A}Valve Turning Flow-Chart}{59}{appendix.A}
\contentsline {chapter}{\numberline {B}Manipulator Modelling In MoveIt!}{60}{appendix.B}
\contentsline {chapter}{\numberline {C}Complementary Results}{68}{appendix.C}
\contentsline {chapter}{\numberline {Bibliography\hspace {-96pt}}}{71}{appendix*.62}
