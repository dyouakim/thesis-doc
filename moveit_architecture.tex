\chapter{MoveIt! Architecture \& Capabilities} \label{ch:moveit}

In this chapter an overview of MoveIt! architecture and the features it provides are presented, taking profit of the knowledge gained during the development of this project.
\\
MoveIt! has the goal of providing an easy-to-use platform to speed up the development of advanced robotics applications. It provides wide functionalities covering several features of the mobile manipulation filed. 
MoveIt! can be seen as plugin-centric framework, as it encapsulates a forward and inverse kinematic solver, planning techniques, and collision detection methods through widely used third-party libraries like: KDL, OMPL, and FCL. To achieve its main goal of extendability and reusability, it also provides plugin interfaces to allow custom configuration of these components, and more higher level capabilities as shown in Fig. \ref{fig:moveit_overview}.

\begin{figure}[ht!]
\centering
  \includegraphics[width=0.6\linewidth]{figures/moveit_high_overview.png}
  \caption{MoveIt! Block Diagram \protect\footnotemark}
  \label{fig:moveit_overview}
\end{figure}
\footnotetext{Extracted from \cite{Coleman2014}}


In Fig. \ref{fig:moveit_architect}, the system architecture is presented. As MoveIt! is ROS based it uses the nodes, messages, and services concept. 
The \textbf{move\_group} is the main node that serves as an integrator: pulling all the individual components together to provide a set of ROS actions and services available to the users.
All these functionalities are accessible through the GUI within RViz, and can be embedded in more complex applications through python or C++ interfaces. Everything is done in a generic way, in order not to concern the users with the low-level components, helping them to focus on the high level application tasks, such as picking up an object or manipulating it.


\begin{figure}[ht!]
  \centering
  \includegraphics[width=.8\linewidth]{figures/moveit_overview.jpg}
  \caption{MoveIt! Architecture \protect\footnotemark}
  \label{fig:moveit_architect}
\end{figure}
\footnotetext{\url{http://moveit.ros.org/}}

The next sections provide a guidance, explaining the steps to be followed in order to setup a new MoveIt! application, integrating most of its capabilities.

\clearpage 

\section{Modelling a new robot}\label{modeling}
In order to start a new application, modelling the system is the first step. This step is divided into two phases as follows:

\begin{itemize}
\item \textit{\underline{\textbf{Defining the URDF file:}}}\\
The \textit{unified robot description format}, has the purpose of providing a robot description in a standard way. A tree structure definition of the robot is defined using tag formats, where all the links and joints parameters are defined. 
\\
The "link" tag is mainly responsible of the physical properties. The physical appearance of the robot can be defined through the link \textless visual\textgreater  field. The look of each link can be defined through typical geometric shapes (e.g. boxes, spheres...), or through the use of a mesh file (both SLT and DAE formats are supported).
The collision property of a link can be defined in its \textless collision\textgreater  field, that will be used later for self-collision check and collision detection during planning. Similarly, for the collision tag, its geometry is specified through simple shapes or using a mesh file.  Fig. \ref{fig:moveit_sample}, shows a sample URDF and the corresponding tree structure as explained in MoveIt! tutorials.

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.7\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/sample_urdf.png}
  \caption{URDF}
  \label{fig:urdf_sample}
\end{subfigure}%
\begin{subfigure}[b]{.3\textwidth}
  \centering
  \includegraphics[width=1.2\linewidth]{figures/sample_tree.png}
  \caption{Tree Structure}
  \label{fig:tree_sample}
\end{subfigure}%
\caption{Four-Links Manipulator Sample}
\label{fig:moveit_sample}
\end{figure}


As shown in the figure, the "joint" tag is the one used to define the system kinematics (e.g. Denavit Hartenberg kinematic parameters), through the  \textless origin\textgreater  field.  Each link (indicated in green in the figure) has a reference frame located at its bottom, and is identical to the reference frame of the joint. So, to add dimensions to the tree, the offset from a link to the joint(s) of its children is to be specified. This is accomplished  by adding the field \textless origin\textgreater  to each of the joints.\\
For example, in Fig. \ref{fig:moveit_sample}, Joint2 is translated in the Y-direction and negative X-direction from link1. It is rotated 90 degrees around the Z-axis.\\
Additionally, different joint types can be defined \textit{(e.g. continuous, revolute, prismatic, planar, fixed, floating)}. A full listing of the important URDF tags and their use, is available in Appendix \ref{ch:urdf_extra}.

\item \textit{\underline{\textbf{Defining the SRDF file:}}}\\
The \textit{semantic robot description format} complements the URDF. It defines what is called \textbf{"planning-groups"}, each group specifies a set of links and joints grouped to be used as a single set for the kinematics solver and the planning algorithms. The kinematic plugin properties  per group (e.g. solver\_timeout, solver\_search\_resolution,...) are configurable. Self-collision check is also configured in this file.
The generation of the SRDF is made easy through the use of \textit{MoveIt! Setup Assistant} (see Fig. \ref{fig:moveit_assistant}). The SRDF file, as well as other configuration files (kinematics.yaml, joint\_limits.yaml, controller.yaml,sensors.yaml) and ROS package files are auto generated through this wizard.
Any changes in the URDF, has to be reflected to the SRDF by running this tool to re-generate the corresponding configuration files.

\begin{figure}[ht!]
\centering
\includegraphics[width=0.55\linewidth]{figures/setup_assistant.png}
\caption{MoveIt! Setup Assistant}
\label{fig:moveit_assistant}
\end{figure}
\end{itemize}

At the end of this step, the system kinematics is defined, and ready to provide forward and inverse kinematics solutions through any selected algorithm. 
Also the robot can be visualized in Rviz using the default launch file generated by the setup assistant. Simple planning queries can be executed in Rviz through the integrated motion planning plugin in order to verify the consistency of the model with the system kinematics.

\section{Kinematics Solver}\label{kdl}
MoveIt! provides a configurable kinematic solver for each planning group. This configuration is specified in the "kinematics.yaml" file. The default plug-in can be replaced by a custom solver through MoveIt! Setup Assistant or by directly changing the yaml file, where the kinematics plugin name and parameters per planning group are specified.
The default  plugin is based on the KDL library that uses numerical techniques to convert from a Cartesian space to a joint configuration space. This library supports generic solvers for kinematic chains, leading to shortages with coupled joints systems. It is also known by its slow performance, and as per MoveIt! experts, it fails frequently when used with a system with less than 6 DoFs.\\
A faster solution can be achieved by utilizing OpenRave’s IKFast plugin that analytically solves the inverse kinematics problem. 
As was shown in Fig. \ref{fig:moveit_architect}, the \textbf{move\_group} node provides direct calls to the kinematic plugin through ROS services. Initially, these services are used in the default MoveIt! planning plug-in. They can also be used to implement custom planning algorithms, integrated with MoveIt!.


\section{Planning \& What Is Behind} \label{sec:plan_behind}
A concept introduced by MoveIt! is the \textbf{PlanningScene}. It represents the complete context necessary for motion planning, and it is wrapped in a single class functionality. 
It allows access to: the robot representation,  the environment representation, collision checking, and constraints evaluation. The \textbf{PlanningScene} and Rviz, are synchronized through publishing \textbf{[PlanningScene]} messages, and it is user responsibility to ensure the synchronization. In the following, each capability is presented:

\begin{itemize}

\item \textbf{Motion Planning Plugin \& Benchmarking:}\label{move_plan}
\\
MoveIt! includes a variety of motion planners: Sampling-based [OMPL], Search-based [SBPL], and Optimization-based [CHOMP].
Each motion planner is integrated as a plugin for MoveIt! which allows also the implementation of custom motion planning algorithms. Motion Planning can be used in two different ways:\\
\begin{itemize}
\item The  \textbf{"MoveGroup" class} and calls to \textit{"plan" and "execute"} methods. An instance of this class is created with the name of the planning group specified.  This planning group defines the set of links and joints to be incorporated in the planning.\\
\item Through the \textbf{planning pipeline} that automatically instantiates a planner plugin, as well as a set of planning request adapters that allow for pre-processing of the planning request or the post-processing of the plans themselves. In this case,  Motion planning request and response are used, and the plan is created through a call to the \textit{"solve"} method.
\end{itemize}

In both cases, the output message of the planning is called \textbf{[moveit\_msg/RobotTrajectory]}. It contains the set of points to be followed to reach the goal. Each point defines position, velocity and acceleration information.\\
As an additional capability, benchmarking is provided to compare different motion planners, kinematic solvers and robot configurations. A warehouse configuration is also available to provide a database of planning scenes, robot states and motion planning problems.

\item \textbf{Environment Representation \& Collision Avoidance:}\\
The environment in the PlanningScene is represented by the \textbf{CollisionWorld} class. It encapsulates the world as a set of objects, either represented by primitive shapes (e.g. spheres, boxes...), or by more complex objects such as an octomap.\\
An essential concept to consider for the collision checking is the \textbf{AllowedCollisionMatrix} included in the \textbf{PlanningScene}. It defines a matrix that specifies if the collision between one entry and the other is enabled/disabled. Initially, it contains the self-collision information (i.e. the model links among themselves) generated by the \textbf{Setup Assistant}. Whenever a collision object is added to the world, it is  convenient to add the corresponding entry in this matrix and specify if collision between this object and each link of the robot model is allowed or not . This is often useful in manipulation tasks where the robot has to move in contact with the world.
The \textbf{Flexible Collision Library} (detailed in \cite{Pan2012}), is integrated with MoveIt! providing collision avoidance using efficient techniques for collision detection and proximity computation. It is based on hierarchical representations and designed to perform multiple proximity queries on different model representations, like discrete and continuous collision detection. It can also perform probabilistic collision checking between noisy point clouds that are captured using cameras or sensors.

\item \textbf{Robot Representation:}\\
Encapsulated in the planning scene, it maintains a full representation of the robot, splitted in two parts:
\begin{itemize}
\item An instance of a \textbf{RobotModel}, which includes the geometry of the robot and its properties (set through the URDF at creation time).

\item  An instance of a \textbf{RobotState}, which specifies the current state of the robot (joint values) for that planning scene. It has to be regularly updated (as explained in the next point), in order to ensure consistency with the real current state of the robot.

\end{itemize}

\item \textbf{Robot State \& Joint State Synchronization:}\label{robot_joint}
\\
The \textbf{RobotState} class has to be synchronized with the real state of the robot,which is achieved by the publication of \textbf{[sensor\_msgs/JointState]} messages. It reads the loaded URDF, finds all non-fixed joints and publishes the corresponding \textbf{[sensor\_msgs/JointState]} message.
The \textbf{Setup Assistant} generates a default publisher node that is used by the default controller. In case of using a custom controller, a corresponding joint state publisher has to be manually defined.
\\
Another useful functionality is the \textbf{RobotState publisher} node generated by the \textbf{Setup Assistant}. It subscribes itself to the JointState publisher node, and publishes a transformation (i.e. translation \& rotation) for each joint state.

\item \textbf{Planning Constraints:}\\
Through the \textbf{PlanningScene}, the robot state can be checked against two types of constraints:
\begin{itemize}
\item A set of kinematic constraints, including joint constraints, position and orientation constraints and visibility constraints.
\item User-specified set of constraints: for instance to check for the stability of a humanoid robot. And it is implemented using a callback function that the user can use to perform any types of checks.
\end{itemize}

\end{itemize}

\section{System Controllers} \label{sec:moveit_controller}
A configurable controller manager plugin is provided by MoveIt! (as shown in Fig.\ref{fig:moveit_overview}), in order to allow the system to perform actions through its actuators. A different controller can be specified per each planning group. To have a working controller plugin, two tasks have to be performed:
\begin{itemize}
\item \textit{Implementation:} \\
In this step, two interfaces have to be implemented. As shown in Fig. \ref{fig:controller_impl}, the first interface \textbf{MoveItControllerManager}, its purpose is to parse the "controller.yaml" file and load the name of each controller specified in the file, as well as managing the activation/deactivation of the controllers. For each controller in the list, it creates a handle.\\
The handle class has to implement \textbf{MoveItControllerHandle} interface (as in the figure), and its main purpose is to implement \textit{"sendTrajectory"} method where the control logic is encapsulated.


\begin{figure}[ht!]
\centering
\includegraphics[width=1.0\linewidth]{figures/controller_impl.jpg}
\caption{MoveIt! Controller Implementation Process}
\label{fig:controller_impl}
\end{figure}


The output message expected from the handle is specified in the yaml file, and commonly there are two types:  [FollowJointTrajectory] and [GripperCommand]. Usually, the first one used for base/torso and similar planning groups, while the second is for grippers planning groups.
This implementation has to be exposed as a plugin through a "plugin.xml" description file.

\item \textit{Configuration:} 

The following auto-generated files (by MoveIt! Setup Assistant), have to be modified to allow MoveIt! planning to communicate with the right controller class at run-time:

\begin{table}[ht!]
\begin{tabular}{|l|l|}
\hline
\multicolumn{1}{|c|}{File Name}  & \multicolumn{1}{c|}{Parameters}                                                                                                                                                   \\ \hline
controller.yaml                  & \begin{tabular}[c]{@{}l@{}}- Specify a controller name per Planning group\\ - Set Joints names per controller\\ - Set the corresponding output message.\end{tabular}                                       \\ \hline
demo.launch                      & \begin{tabular}[c]{@{}l@{}}Move\_Group node parameters have to be changed:\\ -allow\_trajectory\_execution -\textgreater true\\ -fake\_execution -\textgreater false\end{tabular} \\ \hline
move\_group.launch               & Set "moveit\_controller\_manager" to the name of the custom controller.                                                                                                             \\ \hline
trajectory\_execution.launch.xml &                                                                                                                                                                                  \begin{tabular}[c]{@{}l@{}}-The controller launch file (e.g. xxx.launch.xml) is specified.
\\ -The implementation class of the controller to be used.\\ -The path of the controller.yaml config file is specified.\end{tabular} \\ \hline
\end{tabular}
\caption{ Controller Manager Plug-in Configuration Parameters }
\label{table:control_config}
\end{table}
\end{itemize}

\section{Pick Functionality}\label{pickfun}
One of the most important functionality of any manipulator is the grasping capability. MoveIt! provides a special functionality to ease the implementation of this feature. A "Pick" function takes the ID of the object to grasp as well as one or more grasps to try and find successful plan for at least one of them in order for the pick to succeed. The grasp provided is of  type \textbf{[moveit\_msgs/Grasp]} message (shown in Fig.\ref{fig:grasp_all}).

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{0.75\textwidth}
  \centering
  \includegraphics[width=1.2\linewidth]{figures/Grasp.png}
  \label{fig:bad_all}
\end{subfigure}%

\begin{subfigure}[b]{0.75\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/grasp_msg.jpg}
  \label{fig:ee_xyz_2}
\end{subfigure}
\caption{Pick-up Manipulation Pipeline \& Grasp Message Details}
\label{fig:grasp_all}
\end{figure}

The Pickup Manipulation Pipeline consists of three stages ( as shown in Fig. \ref{fig:grasp_all}) which are executed in order of appearance. During these stages different trajectories are added to the final pickup plan. Only if a grasp successfully passes all stages the resulting plan can be executed.\\
The first stage is "Plan Stage" , where the pre-approach trajectory from the starting posture to the pre-grasp posture and an open gripper trajectory are generated and added to the plan.
Then there is an "Approach and Translate Stage" ,where a Cartesian path is calculated starting from the grasp posture into negative the approach direction specified in the message, until the desired approach distance is reached or no inverse kinematic solution is found. If the path is longer than the minimum approach distance specified , then the approach planning is successful and the generated trajectory is added to the plan in the reverse order as the approach trajectory. The first point of this reversed trajectory is the pre-grasp posture. At this stage, the collision between the gripper and the object to grasp is disabled, as well as to any objects specified in \textit{[allow\_to\_touch\_objects]} field in the grasp message.\\
The last stage consists of computing the trajectory to close the gripper, and changing the grasped object status from \emph{world\_object} to \emph{attached}  (i.e. considered part of the robot body). Finally, a Cartesian path from the grasp posture is generated according to the retreat direction. For this trajectory, the minimum retreat distance and the desired retreat distance as specified in the grasp message, are used. The resulting trajectory is then added to the plan.\\
Only If all these stages are successfully generated, the pick operation is ready for execution.
In case of passing more than one grasp message  (e.g. with different approach, retreat, grasp\_pose) to the pick function and more than one successful plan is found, two factors are taken into account to choose which one to execute. First, a quality index called "grasp quality" computed by the user, passed in the grasp message, and internally all the grasps are descendently ordered accordingly to give preference to the one with higher quality. The second, is the plan execution time, as it has not to exceed an internal time-out parameter.

\section{Perception}
This component in MoveIt! deals with the 3D perception through an Occupancy Map Updater. It is also a plugin that can be configured to process different types of input. The currently available plugins in MoveIt! are:
\begin{itemize}
\item The PointCloud Occupancy Map Updater: which can take as input point clouds [sensor\_msgs/PointCloud2]
\item The Depth Image Occupancy Map Updater: which can take as input Depth Images [sensor\_msgs/Image].
\end{itemize}

