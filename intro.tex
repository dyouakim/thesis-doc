\chapter{Introduction} \label{ch:intro}

\section{Background} \label{sec:background}

In the context of this thesis, \textit{Robotics} can be defined as  "The capability of connecting action to perception in an intelligent way, through a control system which command the action, set by a task planning technique in order to achieve a set of goals while satisfying the constraints imposed by the robot and the environment."
Robots can be fixed or mobile. They can be used for survey/inspection and /or intervention applications. According to the environment where they are operated they can be categorized as: \textit{terrestrial (indoor/outdoor), aquatic(surface/underwater) , aerial and spacial}.\\
Mobile robots have the capability of navigating to explore the environment. When a mobile robot is endowed with one or more manipulator(s), they become intervention systems able to interact with the environment.
\\
In the underwater robotics domain, this sort of robots are referenced as \textbf{Underwater Vehicle Manipulator Systems \emph{(UVMS)}} and, depending on their degree of autonomy they are classified as: 1) Work Class  Remotely Operated Vehicles (WROV), 2) Hybrid Remotely Operated Vehicles (HROV), and 3) Intervention Autonomous Underwater Vehicles (I-AUV). This thesis focuses on the study and development of I-AUVs.\\
Possible applications of UVMS include: Assembly, construction, inspection, pick \& place, object transportation, mining disposal, rescue , or diver cooperation.
Another aspect that arises when we talk about mobile manipulators, is \textbf{Obstacle avoidance}. It must be considered for both the robot navigation and the manipulation tasks.
As discussed in \cite{Bohren2011}, even in the well-constrained bounds of a specific application, endowing a personal robot with autonomous capability requires integrating many complex subsystems and capabilities (in perception, motion planning, reasoning, navigation, and grasping...). Though these individual components may have been extensively validated in isolation, seamlessly integrating the components into a robust system is still an active area of research. The challenge is not just in creating robust capabilities, but enabling the extension and reuse of those capabilities in future applications. This challenge created the concept of "app store" paradigm, where a set of basic modules and features are made available. The best way to mange all these modules is through one unified framework, like MoveIt! concept, giving access to broad capabilities and easy extendibility.
\section{Motivation: Underwater Domain Challenges}\label{sect:problem}

The underwater environment is a very difficult one, where fundamental problems such as the localization, the communication or the use of computer vision, are significantly more difficult than within other more conventional robotics domains. For this reason, most of the research in this area has been focused on the navigation, guidance, control and mapping problems. Intervention underwater has been relegated to professional divers or to teleoperated systems. Although during the last two decades, there have been significant advances towards automating the intervention underwater, with milestone projects like SAUVIM, ALIVE and TRIDENT, the underwater intervention technology is still far from the capabilities demonstrated by the state of the art terrestrial robots. This is probably due to the reduced size of the underwater robotics research community dealing with the problem, as well as to the complex and expensive logistics proposed in the early projects (e.g.  SAUVIM vehicles weights 6 Tons and ALIVE weights 3.5 Tons). It is our believe, that using low cost, simple to use test-beds like the \textbf{\emph{Girona500}} I-AUV, will contribute to the further development of the I-AUV technology which is nowadays in its infancy. We also think that to experiment a significant breakthrough in the autonomous underwater intervention capabilities, it is necessary to join efforts with other communities working with mobile-manipulation in other robotic domains, taking profit of their advances.\\ 
\emph{This thesis is a contribution in this direction. It explores how to use the MoveIt! mobile manipulation software, which is being extensively used in the state of the art mobile manipulators, humanoid robots, etc.…,to control and I-AUV for the first time. To the best of our knowledge this is the first work reporting experimental results with a MoveIt! controlled I-AUV.}

\section{Objectives}\label{sec:goals}
The main goal of this thesis is to research the suitability of using the MoveIt! mobile manipulation framework for controlling an Underwater Vehicle Manipulator System (UVMS) to perform autonomous intervention tasks. This goal has been broken down in the following subgoals:
\begin{itemize}
\item \textbf{O1:} Study how to model a redundant UVMS in MoveIt! using an URDF (Unified Robot Description Format) and a SRDF (Semantic Robot Description Format) files. This means to model the kinematics (DoFs, links, joints…) and their shapes and geometries (3D model) to provide MoveIt! with the prior information required to avoid collisions with the environment, as well as self-collisions within robot parts.

\item \textbf{O2:} Design and implement a UVMS controller interface that maps the output of MoveIt! motion-planning to the low-level AUV and manipulator controllers. 

\item \textbf{O3:} Study how to use the workspace and reachability analysis to plan a grasp action while optimizing the UVMS manipulability index.

\item \textbf{O4:} Demonstrate experimentally the performance of a MoveIt! based development to solve the “Free-floating Valve Turning” benchmark in a water tank using the \textbf{\emph{Girona500}} 8 DoF UVMS.
\end{itemize}
\section{Outline}\label{sec:outline}

The organization of the present document goes  as follows: \\
\textbf{Chapter 2}  presents a detailed state of the art in the underwater robotics field. This survey was essential to give insight about what has been previously done in the field.\\
\textbf{Chapter 3} gives a brief overview of the I-AUV hardware and software used in this thesis. MoveIt! had to be integrated with this system to perform intervention tasks.\\
\textbf{Chapter 4} introduces MoveIt! framework architecture and its capabilities, to give an insight about the features implemented in this project which are presented later.\\
\textbf{Chapter 5} is the core of this document as it contains details of the research and work done, explaining how to use MoveIt! and how to integrate it with the existing I-AUV system. It also describes the details of the intervention scenario used in the experimental work.\\

\textbf{Chapter 6} explains the methodology followed during all experimental phases of the project, along with the results and their analysis.
 \\
Finally, \textbf{Chapter 7} conclude the work done and proposes future work. 
