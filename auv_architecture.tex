\chapter{I-AUV Architecture} \label{ch:AUV}

In this chapter, details of the existing I-AUV system (hardware components as well as its software architecture) are introduced.

\section{Hardware Overview}

The \textbf{\textit{Girona 500}} (as detailed in \cite{David2011}), is a compact-size I-AUV with hovering capabilities, designed for a maximum operating depth of 500 m. The vehicle is built around an aluminum frame which supports three torpedo-shaped hulls as well as other elements like the thrusters. The overall dimensions of the vehicle are 1 m in height, 1 m in width, 1.5 m in length and a weight (on its basic configuration) of about 140 Kg. 
The two upper hulls, which contain the flotation foam and the electronics housing, are positively buoyant, while the lower one contains the more heavy elements such as the batteries and the payload. This particular arrangement of the components provides the vehicle with passive stability in pitch and roll, making it suitable for tasks requiring a stable platform such as video surveying or intervention. The most remarkable characteristic of the Girona 500 I-AUV is its capacity to reconfigure for different tasks. On its basic configuration, the vehicle is equipped with typical navigation sensors (DVL, AHRS, pressure gauge and USBL) and a basic survey equipment (profiler sonar, side scan sonar, video camera and sound velocity sensor).

\begin{figure}[ht!]
\centering
\includegraphics[width=0.7\linewidth]{figures/Girona500_arm.png}
\caption{On the right, the Girona 500 I-AUV. On the left, a 3D model of the customized end-effector, with three blocks: 1 passive gripper, 2 camera in-hand \& 3 F/T sensor.}
\label{fig:girona500}
\end{figure}

To perform intervention tasks, the Girona 500 I-AUV is equipped with an under-actuated manipulator  with 4 DoFs (slew, elbow, elevation and roll) and a custom end-effector (see Fig \ref{fig:girona500}). The custom end-effector is composed of three different parts . The first
one is a V-shape compliant passive gripper to absorb small impacts. The second element consists of a camera in hand which has been installed in the center of the gripper to provide a visual feedback of what the end-effector is manipulating. It has been placed to prevent the occlusion of the vehicle’s camera by the manipulator. Finally, a F/T sensor, provides information about the quality of the grasping and the torque applied during the intervention task.

\section{Software Overview} \label{sec:girona500_sw_overview}

\textbf{\textit{Girona 500}}'s system architecture, termed \emph{COLA2} (Component Oriented Layer based Architecture for Autonomy), allows to control the AUV during survey and intervention missions. A block diagram describing the architecture is shown in Fig \ref{fig:girona500_arch}. The architecture consists of a vehicle interface module with device drivers for sensors and actuators in connection with a perceive-plan-act (PPA) module. The PPA module constructs a representation of the world using sensor measurements including images, point clouds and ranges. An estimate of the robot's trajectory is maintained by the localization component, and this estimate is used by the Object Recognition and Mapping components.
In the below, a brief description of the main components is presented:


\begin{figure}[ht!]
\centering
\includegraphics[width=0.7\linewidth]{figures/girona500_arch_overview.png}
\caption{Girona500 System Architecture}
\label{fig:girona500_arch}
\end{figure}

\begin{itemize}
\item \textbf{Localization:}\\
The AUV localization component (as explained in \cite{Narcis2014}), is based on an extended Kalman Filter (EKF), that merges standard navigation sensors with vision-based updates. The filter is in charge of estimating the vehicle's position $([x\,y\,z])$ and linear velocity $([u\,v\,w])$. While, the vehicle orientation $([\phi\,\theta\,\psi]))$ and angular velocity $([p\,q\,r])$ are directly measured by an attitude and heading reference system (AHRS). This filter is also able to map the pose in the world of several landmarks, thus, working as a simultaneous localization and mapping (SLAM) algorithm. Despite the filter is designed to deal with several landmarks, in the context of this project, a single landmark is used (i.e. a panel).
\\
The filter prediction is based on a constant velocity kinematics model, and with the assumption that Landmark(s) are static.
\\
When only velocity updates are available, the filter behaves as a dead-reckoning algorithm that drifts over time. However, if position updates or landmarks are detected, the localization filter is able to keep its error bounded.

\item \textbf{Navigation \& Control:}\\
 The AUV can be controlled by means of body force requests $([X^\prime;Y^\prime;Z^\prime;N^\prime])$, body velocity requests $([u^\prime;v^\prime;w^\prime;r^\prime])$ and waypoint request $([x^\prime;y^\prime;z^\prime;\psi^\prime])$.
A cascade control scheme is used to link these controllers (as in Fig. \ref{fig:girona500_arch}).
The first controller is a 4 DoF proportional-integral-derivative (PID) called pose controller. It receives as input the current vehicle pose $([x; y; z;\psi])$ and a desired pose $([x^\prime;y^\prime;z^\prime;\psi^\prime])$. Its output is a desired velocity ($v^\prime$).
This velocity is then sent to the velocity controller together with vehicle $\prime$s current velocity ($v$) following the cascade scheme. The velocity controller computes the desired force and torque by combining a 4 DoF PID  with an open loop model-based controller.  The output of this velocity controller is the desired force and torque $(\tau^\prime)$ defined in the vehicle\'s body frame. \\
To obtain the force that each thruster has to generate this $\tau^\prime$ is multiplied by a thruster allocation matrix. Next, a simple thruster model is applied to transform thruster forces into thruster set-points.
\\
Two motion modes are provided to guide the vehicle. The first one follows the whole cascade scheme moving the AUV holonomically by sending way-point requests to the pose controller.
The second, is based on the variation of line-of-sight (LOS) pure pursuit guidance. The orientation error $(\psi_{e})$ between vehicle's current pose and the desired way-point is computed, and sent to the pose controller together with the desired depth $(z^{\prime})$.
When $(\psi_{e})$ is below a limit, a velocity $v^\prime$ is sent to the velocity controller.

\item {\textbf{Object Recognition:}}\\
The Object Recognition component uses a priori knowledge to seek for matching between sensor measurements and object models.
A vision-based system identifies the specified object (e.g. the panel) and computes its relative position. This information is then introduced in the localization filter as a landmark and is updated with successive observations.
The system performs the detection by comparing the images from the camera against a priori known template of this object. By detecting and matching unique features in the camera image and template, it is possible to detect its presence, as well as accurately estimate its position/orientation when a  sufficient number of features are matched.
A minimum number of keypoints must be matched between the template and the camera image to satisfy the landmark detection requirement.

\end {itemize}

