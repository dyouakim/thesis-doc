\chapter{Experimental Setup \& Results} \label{ch:setup}

This chapter explains in details the methodology followed during the development of the project and the results obtained.

\section{Experiments Methodology}\label{sec:methodology}
The goal of this section is to explain the methodology followed and the tools used to validate each step in the development in order to achieve the main objective mentioned in \ref{sec:goals}.
\\
Since the main objective was to use MoveIt! framework in the underwater field, three broad phases of development proceeded by testing and validation were  followed. At each phase a new level of validation was applied in order to achieve the final experiment in the water tank using the real system (\textbf{\emph{Girona500}} equipped with the 4 DoFs arm).

\subsection{MoveIt! \& Rviz Simulation}
This was the initial phase, where the purpose was to introduce the system to MoveIt! and start the validation of different MoveIt! capabilities in simulation (e.g. simple planning to a target pose, modifying the robot environment, grasp virtual objects...).
\\
In this phase Rviz (3D visualization tool for ROS) was used, as it is easily integrated with MoveIt! through the Rviz plug-in. The result of this step is creating  the 3D model of the system, visualizing it in Rviz, as well as its collision model, and its kinematics parameters.

\subsection{UWSim Environment}
UWSim is an UnderWater SIMulator for marine robotics research and development. UWSim started with the RAUVI and TRIDENT research projects as a tool for testing and integrating perception and control algorithms before running them on the real robots.
This tool is widely used in different projects in CIRS lab, and a version configured to the system used in this project was provided as shown in Fig.[\ref{fig:uwsim}]

\begin{figure}[ht!]
\centering
\includegraphics[width=0.3\linewidth]{figures/UWsim.png}
\caption{The valve turning environment modelled in UWSim}
\label{fig:uwsim}
\end{figure}

The purpose of this phase is to simulate the real system as accurate as possible, and use MoveIt! framework to control the system, observe the results in UWSim and ensure synchronization between both tools. The synchronization on the level of the system model and kinematics as well as the control level.  The MoveIt! controller plug-in was developed and its interface with the real system controller was tested on simple planning tasks as well as on the full valve turning intervention scenario.

\subsection{The Real System In The Water Tank}

The CIRS building has a ${16x5x8\: m^{3}}$ water tank (see Fig. [\ref{fig:watertank}]) to do small experiments with underwater vehicles.
And this is where the final valve turning intervention scenario was tested on the real system.

\begin{figure}[ht!]
\centering
\includegraphics[width=0.55\linewidth]{figures/WaterTank.png}
\caption{CIRS Water Tank}
\label{fig:watertank}
\end{figure}

Some assumptions and considerations had to be taken into account in order to have successful navigation/manipulation scenario in the tank:
\begin{itemize}
\item Since no scanning system was integrated to build ocotmaps of the environment, the (X,Y,Z) joints of the AUV were limited to a bounding box within the dimension of the tank in order to keep the safety of the system during navigation.
\item In UWSim it is easy to assume the \textit{North} direction is ahead of the vehicle up (positive X of the AUV reference frame), but in the real tank, the \textit{real North} is rotated with respect to this reference, and its value is variable from one experiment to the other (ranges between 0.7 and 0.9), due to magnetic perturbations produced by the water tank walls while the AHRS is initialized. So, its value has to be configured at the beginning of each experiment  (through a config file). This offset transformation is applied to any point manually defined in the simulation (e.g the detection points, virtual obstacles location...).
\item As mentioned earlier the roll and pitch of the AUV initially were not modelled as they can not be actuated, but due to the floating effect and water disturbance, the AUV can experiment changes in roll and pitch and that is different from time to time. So, these values have to be measured and set at the beginning of the experiment to ensure a better navigation accuracy.
\item Another important aspect that can highly affect the success of the valve turning is the arm calibration. Initially, the arm has to be calibrated, and with the repetition of the experiments and with the gripper touching the valve, it may need re-calibration to ensure the force applied to the panel/valve did not affect it, specially for the roll of the wrist joint.
\end{itemize}

\section{Results \& Analysis}\label{sec:results}
In this section, the results obtained in each of the previously mentioned phase, are detailed. The main focus is the analysis of the valve turning intervention task performed in the real environment.  Several scenarios have been tested in the water tank incorporating all the implemented features.

\subsection{MoveIt! \& Rviz Simulation Results}

As explained in the methodology, the purpose of this phase is to verify the modelling in MoveIt! and test simple planning and visualize the result in Rviz with the fake controller before connecting the implemented system with UWSim. Fig. \ref{fig:moveit_initial_result}] shows the results of this initial tests. Fig. \ref{fig:moveit_result_start}, the start and goal poses are shown, the goal pose is in orange. While, Fig.\ref{fig:moveit_result_plan} shows the plan produced by MoveIt! planning plug-in with animated trail showing the progress of the motion from the start to the goal pose. These results can be displayed using the motion planning plug-in visualized in Rviz as presented in Fig. \ref{fig:moveit_initial_result}.

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/moveit_result_start.png}
  \caption{Start \& End Poses}
  \label{fig:moveit_result_start}
\end{subfigure}%
\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/moveit_result_plan.png}
  \caption{The Output Plan}
  \label{fig:moveit_result_plan}
\end{subfigure}

\begin{subfigure}[b]{0.6\textwidth}
  \centering
  \includegraphics[width=0.95\linewidth]{figures/moveit_result_motion_planning.png}
  \caption{Motion Planning Plug-in In MoveIt!}
  \label{fig:moveit_result_motion_planning}
\end{subfigure}%
\caption{Initial Planning Tests in MoveIt! \& Rviz}
\label{fig:moveit_initial_result}
\end{figure}
 

\subsection{Water Tank Experiment Results} \label{exp_result_sec}
In this section, the results of the experiments done in the water tank are presented, and the UWSIM results were omitted. They were an essential verification step but do not have an additional value in their own. 
Due to the challenges and difficulties doing the experiments in the water tank, the tests were done incrementally (with increasing difficulty), in order to ensure the quality of the results.
So initially, the focus was to validate if the valve turning is successful. The purpose was only to validate it using the two different implemented techniques  (i.e. normal plan \& pick pipeline).
Scenario(1) and scenario (2) presented below corresponds to these tests. Only one detection point, oriented to the panel, was tested along with the inspection point, and the plan or the pick to perform the valve turning. No obstacle avoidance was included in the first two tests.\\
Then scenario (3) was concerned with incorporating obstacle avoidance. For this purpose, multiple detection points were added as in Fig. \ref{fig:all_det}. The I-AUV was required to navigate between them with an obstacle on the way. Finally, the purpose of scenario(4) was to validate the importance of the workspace analysis. The scenario included one detection point oriented to the panel, but with a virtual obstacle close to the panel (as in Fig. \ref{fig:obstacles_scene}). As a result, it forces a different posture (not the orthogonal one) for the I-AUV to perform the manipulation task. In all scenarios, valve\#2 (see Fig. \ref{fig:panel_axis}) was the target.
Table  \ref{testing_checklist} lists the tests validated in the  water tank, along with the features enabled during the test. The inspection points were omitted from the table, as they were not changing during all the tests.

\begin{table}[h]
\begin{tabular}{|c||c|c|c|c|}
\hline
Scenario\# & Detection Points                  & Obstacles                        & \begin{tabular}[c]{@{}c@{}}Grasp \\ Method\end{tabular} & \begin{tabular}[c]{@{}c@{}}Workspace \\ Analysis\end{tabular} \\ \hline \hline
(1)        & One Point & None                             & Plan \& Way Points                                      & No                                                            \\ \hline
(2)        & One Point  & None                             & Pick                                                    & No                                                            \\ \hline
(3)        & Three points                      & One between the detection points & Plan \& Way Points                                      & No                                                            \\ \hline
(4)        & One Point  & One close to the panel           & Pick                                                    & Yes                                                           \\ \hline
\end{tabular}
\caption {CheckList Of The Tests  Done In The Water Tank}
\label{testing_checklist}
\end{table}


In the upcoming sections, the results per scenario are presented. Each scenario is labeled as in Table \ref{testing_checklist}, and the same set of graphs and Rviz resutls are presented for each one, with five sets per scenario as follow:
\begin{itemize}
\item \textbf{Set(1):} In this set, the AUV and the End-effector trajectories in 3-D are presented on the right. The left figure displays the path followed along with the environment as it appeares in Rviz. The yellow markers represent the AUV motion, while the purple ones represent the End-Effector. The green objects represent the world objects, all of them added to avoid collision, with the exception of the grasping object (the green cube) located on the upper valve. The arrow markers represent the different way-points followed during the trajectory. The red arrow indicates the detection point, the brown is related to the inspection way-point, the blue represents the approach, and the yellow for the grasp \& turn.
The purpose of this set is to show the path followed by the system during different phases, and to ensure it went through the requested way points.

\item \textbf{Set(2):}  is aiming to show the different configurations taken by the whole I-AUV during the trajectory (from the start until the manipulation is completed).

\item \textbf{Set(3):} the target of this set is to show the evolution of the 4 DoFs of the AUV during the whole scenario, and to verify that the specified controller tolerance is respected. In order to have an accurate verification of the trajectory followed, both the actual path and the required one generated from the controller are needed. Unfortunately, for this set of joints only the actual and the planned were recorded. The planned is MoveIt! planner output, thus it does not have the notion of time, and it just represents intermediate way points to be followed to reach the goal (example in Fig. \ref{fig:plannedZ}). The presented set of graphs was generated by visually mapping the planning output with the actual given the execution time of the whole scenario. This leads to inaccuracy in the plots compared to the arm joints ones. \\
In this set, the bold red represents the planned path, and the blue one shows the actual path. The light red line up and down shows the acceptance boundary specified to the controller. This error is [6cm] for $(X,Y)$, [0.06rad] for $\psi$, and [2cm] for $Z$.\\ 
A noticeable behaviour in most of the graphs is a discontinuity in the planned path. It is due to the fact that between consecutive execution of two plans, there is a planning time (taken by MoveIt! to generate the plan), during which no commands are sent to the AUV controller, leading to possible drifts or floating effect. So the next execution of a plan will not starts where the previous plan ended. This discontinuity is obvious in some DoF more than others, specially for the depth (i.e. Z). Whenever a discontinuity is encountered in the graph, it is marked by a black rectangle to show that no control activity is performed during this period.

\item \textbf{Set(4):} shows the evolution of the 4 DoFs arm joints (i.e. shoulder, biceps, elbow, wrist). Unlike the previous set, for the arm the actual and the required were both recorded during the experiments. So, the plots obtained are more accurate. The actual followed path is shown in blue, while the required is in bold red. The two upper and lower red lines represent the accepted controller tolerance. It is [0.075rad] for $[q1 q2 q3]$, and [0.3rad] for $q4$ (i.e. the wrist). On the wrist joint evolution graph, the moment of turning the valve can be clearly seen at the end of the execution, showing the success of the manipulation task.

\item \textbf{Set(5):} the last set, it presents both End-Effector position $(X,Y, Z)$ and orientation $(roll,pitch,yaw)$. They are shown separately in a plane (with the corresponding R,G,B colours), along with the set points that had to be achieved in each phase as bold circles (their X,Y,Z are coloured in Magenta, Yellow and Cyan). The vertical black dotted line shows the boundary of each phase. In these two graphs, we can verify that the end-effector is reaching the required position and orientation accurately or within small error. The verification is performed during the inspection \& grasping phases as these points were defined as end-effector pose. The same can not be verified for the detection through the end-effector trajectory, since only the base of the AUV was required to move by directly specifying $(X,Y,Z,\psi)$ point.
\end{itemize}

\clearpage
\begin{itemize}

\item \textbf{\textit{\underline{Scenario(1):}}}\\

\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/scenario1_3dplots.png}
  \caption{End-Effector \& AUV positions in NED-Frame}
  \label{fig:scenario1_3d}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/scenario1_fullpath.png}
  \caption{End-Effector (purple) \& AUV (yellow) positions in Rviz}
  \label{fig:scenario1_rviz}
\end{subfigure}
\caption{The valve turning scenario(1)}
\label{fig:scenario1}
\end{figure}

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.31\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/scenario1_start.png}
  \caption{"Start"}
  \label{fig:scenario1_s}
\end{subfigure}%
\begin{subfigure}[b]{.32\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/scenario1_detection.png}
  \caption{"Detection"}
  \label{fig:scenario1_d}
\end{subfigure}
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/scenario1_inspection.png}
  \caption{ "Inspection"}
  \label{fig:scenario1_i}
\end{subfigure}

\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/scenario1_way1.png}
  \caption{"Approach"}
  \label{fig:scenario1_i}
\end{subfigure}%
\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/scenario1_grasp.png}
  \caption{ "Grasp \& Turn" }
  \label{fig:scenario1_g}
\end{subfigure}%
\caption{The valve turning scenario(1): Configuration Evolution During The Trajectory}
\label{fig:scenario1_phases}
\end{figure}

\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/x_1.png}
  \caption{AUV - X Position Planned vs. Actual}
  \label{fig:scenario1_x}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/y_1.png}
  \caption{AUV - Y Position Planned vs. Actual}
  \label{fig:scenario1_y}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/z_1.png}
  \caption{AUV - Z Position Planned vs. Actual}
  \label{fig:scenario1_z}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/yaw_1.png}
  \caption{AUV - Yaw  Planned vs. Actual}
  \label{fig:scenario1_yaw}
\end{subfigure}
\caption{Scenario(1): AUV Pose Evolution}
\label{fig:scenario1_auv}
\end{figure}

\clearpage

\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/sh_1.png}
  \caption{Arm - Shoulder Joint Planned vs. Actual}
  \label{fig:scenario1_sh}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/bi_1.png}
  \caption{Arm - Biceps Joint Planned vs. Actual}
  \label{fig:scenario1_bi}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/el_1.png}
  \caption{Arm - Elbow Joint Planned vs. Actual}
  \label{fig:scenario1_el}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/w_1.png}
  \caption{Arm - Wrist Joint Planned vs. Actual}
  \label{fig:scenario1_w}
\end{subfigure}
\caption{Scenario(1): Arm Joints Evolution}
\label{fig:scenario1_arm}
\end{figure}



\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.4\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/EE_XYZ_1.png}
  \caption{End-Effector Position}
  \label{fig:ee_xyz_1}
\end{subfigure}
\begin{subfigure}[b]{.4\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/EE_RPY_1.png}
  \caption{End-Effector Orientation}
  \label{fig:ee_rpy_1}
\end{subfigure}%
\caption{Scenario(1): Actual EE-XYZ \& EE-RPY marked in Red, Green, Blue \\
With Corresponding Set points marked in Magenta, Yellow, Cyan}
\label{fig:scenario1_others}
\end{figure}

It is clearly seen in both Fig. \ref{fig:ee_xyz_1} and \ref{fig:ee_rpy_1}, that the end-effector went through the requested points. Also from Fig. \ref{fig:scenario1_w}, the $-\pi$ change in the wrist roll at the end indicates the turning action.



\item \textbf{\textit{\underline{Scenario(2):}}}\\

\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/scenario2_3dplots.png}
  \caption{End-Effector \& AUV positions in NED-Frame}
  \label{fig:scenario2_3d}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/scenario2_fullpath.png}
  \caption{End-Effector (purple) \& AUV (yellow) positions in Rviz}
  \label{fig:scenario2_rviz}
\end{subfigure}
\caption{The valve turning scenario(2)}
\label{fig:scenario2}
\end{figure}

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/scenario2_start.png}
  \caption{"Start"}
  \label{fig:scenario2_s}
\end{subfigure}%
\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/scenario2_detection.png}
  \caption{"Detection"}
  \label{fig:scenario2_d}
\end{subfigure}

\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/scenario2_inspection.png}
  \caption{"Inspection"}
  \label{fig:scenario2_i}
\end{subfigure}%
\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/scenario2_grasp.png}
  \caption{"Grasp \& Turn"}
  \label{fig:scenario2_g}
\end{subfigure}%
\caption{The valve turning scenario(2): Configuration Evolution During The Trajectory}
\label{fig:scenario2_phases}
\end{figure}

\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/x_2.png}
  \caption{AUV - X Position Planned vs. Actual}
  \label{fig:scenario2_x}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/y_2.png}
  \caption{AUV - Y Position Planned vs. Actual}
  \label{fig:scenario2_y}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/z_2.png}
  \caption{AUV - Z Position Planned vs. Actual}
  \label{fig:scenario2_z}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/yaw_2.png}
  \caption{AUV - Yaw  Planned vs. Actual}
  \label{fig:scenario2_yaw}
\end{subfigure}
\caption{Scenario(2): AUV Pose Evolution}
\label{fig:scenario2_auv}
\end{figure}

\clearpage
\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/sh_2.png}
  \caption{Arm - Shoulder Joint Planned vs. Actual}
  \label{fig:scenario2_sh}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/bi_2.png}
  \caption{Arm - Biceps Joint Planned vs. Actual}
  \label{fig:scenario2_bi}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/el_2.png}
  \caption{Arm - Elbow Joint Planned vs. Actual}
  \label{fig:scenario2_el}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/w_2.png}
  \caption{Arm - Wrist Joint Planned vs. Actual}
  \label{fig:scenario2_w}
\end{subfigure}
\caption{Scenario(2): Arm Joints Evolution}
\label{fig:scenario2_arm}
\end{figure}


\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.4\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/EE_XYZ_2.png}
  \caption{End-Effector Position}
  \label{fig:ee_xyz_2}
\end{subfigure}
\begin{subfigure}[b]{.4\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/EE_RPY_2.png}
  \caption{End-Effector Orientation}
  \label{fig:ee_rpy_2}
\end{subfigure}%
\caption{Scenario(2): Actual EE-XYZ \& EE-RPY marked in Red, Green, Blue \\
With Corresponding Set points marked in Magenta, Yellow, Cyan}
\label{fig:scenario2_others}
\end{figure}

As in the previous scenario, we can see from both Fig. \ref{fig:ee_xyz_2} and \ref{fig:ee_rpy_2}, that the end-effector went through the requested points. Also from Fig. \ref{fig:scenario2_w}, the $-\pi$ change in the wrist roll at the end indicates the turning action. Even the start orientation was at $-\pi$, it was able during the path to reach the $0$ (leading to the steps in the figure), before reaching the turn pose.\\
The most noticeable difference compared to the previous scenario is that the path generated by the pick pipeline is longer than using normal planing with intermediate way points. Most likely this is due to MoveIt! internal handling to reach all pipeline generated points with high accuracy. Both paths looks quiet similar as they follow same way-points but with different approaches.

\item \textbf{\textit{\underline{Scenario(3):}}}\\

\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/scenario3_3dplots.png}
  \caption{End-Effector \& AUV positions in NED-Frame}
  \label{fig:scenario3_3d}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/scenario3_fullpath.png}
  \caption{End-Effector (purple) \& AUV (yellow) positions in Rviz}
  \label{fig:scenario3_rviz}
\end{subfigure}
\caption{The valve turning scenario(3)}
\label{fig:scenario3}
\end{figure}

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.31\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/scenario3_start.png}
  \caption{"Start"}
  \label{fig:scenario3_s}
\end{subfigure}%
\begin{subfigure}[b]{.32\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/scenario3_detection.png}
  \caption{"Detection"}
  \label{fig:scenario3_d}
\end{subfigure}
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/scenario3_inspection.png}
  \caption{"Inspection"}
  \label{fig:scenario3_i}
\end{subfigure}

\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/scenario3_way1.png}
  \caption{ "Approach"}
  \label{fig:scenario3_i}
\end{subfigure}%
\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/scenario3_grasp.png}
  \caption{"Grasp \& Turn" }
  \label{fig:scenario3_g}
\end{subfigure}%
\caption{The valve turning scenario(3): Configuration Evolution During The Trajectory}
\label{fig:scenario3_phases}
\end{figure}

\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/x_3.png}
  \caption{AUV - X Position Planned vs. Actual}
  \label{fig:scenario3_x}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/y_3.png}
  \caption{AUV - Y Position Planned vs. Actual}
  \label{fig:scenario3_y}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/z_3.png}
  \caption{AUV - Z Position Planned vs. Actual}
  \label{fig:scenario3_z}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/yaw_3.png}
  \caption{AUV - Yaw  Planned vs. Actual}
  \label{fig:scenario3_yaw}
\end{subfigure}
\caption{Scenario(3): AUV Pose Evolution}
\label{fig:scenario3_auv}
\end{figure}

\clearpage

\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/sh_3.png}
  \caption{Arm - Shoulder Joint Planned vs. Actual}
  \label{fig:scenario3_sh}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/bi_3.png}
  \caption{Arm - Biceps Joint Planned vs. Actual}
  \label{fig:scenario3_bi}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/el_3.png}
  \caption{Arm - Elbow Joint Planned vs. Actual}
  \label{fig:scenario3_el}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/w_3.png}
  \caption{Arm - Wrist Joint Planned vs. Actual}
  \label{fig:scenario3_w}
\end{subfigure}
\caption{Scenario(3): Arm Joints Evolution}
\label{fig:scenario3_arm}
\end{figure}

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.4\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/EE_XYZ_3.png}
  \caption{End-Effector Position}
  \label{fig:ee_xyz_3}
\end{subfigure}
\begin{subfigure}[b]{.4\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/EE_RPY_3.png}
  \caption{End-Effector Orientation}
  \label{fig:ee_rpy_3}
\end{subfigure}%
\caption{Scenario(3): Actual EE-XYZ \& EE-RPY marked in Red, Green, Blue \\
With Corresponding Set points marked in Magenta, Yellow, Cyan}
\label{fig:scenario3_others}
\end{figure}

Fig. \ref{fig:ee_xyz_3} and \ref{fig:ee_rpy_3} show how the end-effector went through the requested points. Also from Fig. \ref{fig:scenario3_w}, the $-\pi$ change in the wrist roll at the end indicates the turning action. Aside from the obstacle avoidance enabled when navigating from the starting position to the first detection point. This scenario shows the importance of the inspection points. As the panel was detected on the way going to the detection point, the estimation of the panel pose on the way was not so accurate. Thus, forcing the I-AUV pose oriented to the estimated panel pose, enables getting better estimation of the panel pose (i.e. valve pose), and a correction to the panel and valve objects poses was applied before grasping. 
Complementary results showing the path between all detection points with and without obstacle is presented in Appendix \ref{ch:result_extra}. 
\\
Another observation, specially in Fig. \ref{fig:scenario3_z}, is the high fluctuation of the AUV depth compared to previous scenarios. Here we see that it goes within the boundary, and instantly drifts. Since this experiment was not done in the same day as the previous ones, it is hard to analyse the reason behind. Specially, that there are so many factors that change from one experiment to the other. Possible reasons could be related to the AUV configurable weight that can lead to more floating or sinking effect.
\clearpage
\item  \textbf{\textit{\underline{Scenario(4):}}}

\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/scenario4_3dplots.png}
  \caption{End-Effector \& AUV positions in NED-Frame}
  \label{fig:scenario4_3d}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/scenario4_fullpath.png}
  \caption{End-Effector (purple) \& AUV (yellow) positions in Rviz}
  \label{fig:scenario4_rviz}
\end{subfigure}
\caption{The valve turning scenario(4)}
\label{fig:scenario2}
\end{figure}

Fig. \ref{fig:workspace_chosen} shows in Rviz the grasp pose (yellow arrow) chosen by the workspace analysis. It is not the same orthogonal orientation, thus showing the benefit of the workspace analysis feature specially in tight manipulation locations. The change of the location and/or the dimensions of the obstacles will lead to different suitable orientation.

\begin{figure}[ht!]
\centering
\includegraphics[width=0.28\linewidth]{figures/workspace_best.png}
\caption{Grasp Orientation Selected By The Workspace Analysis}
\label{fig:workspace_chosen}
\end{figure}


\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.95\linewidth]{figures/scenario4_start.png}
  \caption{"Start"}
  \label{fig:scenario4_s}
\end{subfigure}%
\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.95\linewidth]{figures/scenario4_detection.png}
  \caption{"Detection"}
  \label{fig:scenario4_d}
\end{subfigure}

\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.95\linewidth]{figures/scenario4_inspection.png}
  \caption{"Inspection"}
  \label{fig:scenario4_i}
\end{subfigure}%
\begin{subfigure}[b]{.35\textwidth}
  \centering
  \includegraphics[width=0.95\linewidth]{figures/scenario4_grasp.png}
  \caption{"Grasp \& Turn"}
  \label{fig:scenario4_g}
\end{subfigure}%
\caption{The valve turning scenario(4): Configuration Evolution During The Trajectory}
\label{fig:scenario4_phases}
\end{figure}


\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/x_4.png}
  \caption{AUV - X Position Planned vs. Actual}
  \label{fig:scenario4_x}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/y_4.png}
  \caption{AUV - Y Position Planned vs. Actual}
  \label{fig:scenario4_y}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/z_4.png}
  \caption{AUV - Z Position Planned vs. Actual}
  \label{fig:scenario4_z}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/yaw_4.png}
  \caption{AUV - Yaw  Planned vs. Actual}
  \label{fig:scenario4_yaw}
\end{subfigure}
\caption{Scenario(4): AUV Pose Evolution}
\label{fig:scenario4_auv}
\end{figure}


\begin{figure}[ht!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/sh_4.png}
  \caption{Arm - Shoulder Joint Planned vs. Actual}
  \label{fig:scenario4_sh}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/bi_4.png}
  \caption{Arm - Biceps Joint Planned vs. Actual}
  \label{fig:scenario4_bi}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.85\linewidth]{figures/el_4.png}
  \caption{Arm - Elbow Joint Planned vs. Actual}
  \label{fig:scenario4_el}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.85\linewidth]{figures/w_4.png}
  \caption{Arm - Wrist Joint Planned vs. Actual}
  \label{fig:scenario4_w}
\end{subfigure}
\caption{Scenario(4): Arm Joints Evolution}
\label{fig:scenario4_arm}
\end{figure}


\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.4\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/EE_XYZ_4.png}
  \caption{End-Effector Position}
  \label{fig:ee_xyz_3}
\end{subfigure}
\begin{subfigure}[b]{.4\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/EE_RPY_4.png}
  \caption{End-Effector Orientation}
  \label{fig:ee_rpy_3}
\end{subfigure}%
\caption{Scenario(4): Actual EE-XYZ \& EE-RPY marked in Red, Green, Blue \\
With Corresponding Set points marked in Magenta, Yellow, Cyan}
\label{fig:scenario3_others}
\end{figure}

\clearpage
In this scenario, the workspace analysis was integrated and performed online. When the inspection point is reached, a call to the workspace analysis service is performed, and the I-AUV is in a waiting state. This period of time is indicated in all the graphs of Fig. \ref{fig:scenario4_auv} by dotted black lines. We can see that the grasp analysis lasts for almost five minutes, showing a draw-back of the online workspace analysis.
This waiting effect was also obvious when the MoveIt! planner output was observed (see Fig. \ref{fig:plannedZ}). The amount of drift  in the depth is indicated by the red arrow. \\
The AUV joints, specially the depth suffers fluctuations similar (or even more) to the previous scenario, as it was done in the same day after scenario(3) experiment.
\begin{figure}[ht!]
\centering
\includegraphics[width=0.65\linewidth]{figures/4_plannedZ.png}
\caption{Moveit! Output Plan For The AUV-Z Position}
\label{fig:plannedZ}
\end{figure}
 
\end{itemize}
