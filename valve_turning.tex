\chapter{MoveIt! Based Sea-Intervention Application} \label{ch:valve_turn_moveit}

In this chapter, the details of importing MoveIt! capabilities into the existing I-AUV system are explained, followed by the technical details of the intervention scenario chosen to validate the implemented system.

\section{Implemented Software Architecture \& Capabilities}\label{sect:software}

The system is implemented in ROS and C++, Fig. \ref{fig:arch} gives an overview of the different nodes, the interaction between them and with the I-AUV controller. Also showing the ROS services and messages used to exchange information between different parts of the system.

\begin{figure}[ht!]
\centering
\includegraphics[width=1.2\linewidth]{figures/system_nodes.jpg}
\caption{Implemented System Architecture \& Interface with the I-AUV}
\label{fig:arch}
\end{figure}

The main node is the \emph{\textbf{PlanningManager}} that encapsulates the planning capabilities and all related \emph{\textbf{PlanningScene}} features. The controller plugin (implemented as explained in section \ref {sec:moveit_controller}) interfaces with the I-AUV controller, in order to give commands to the system actuators. The synchronization node has the role of updating MoveIt! joint state with the I-AUV joint state (as explained in section \ref{sec:plan_behind}). The rest of the nodes are specific to the implemented intervention scenario and will be explained in section \ref{sec:valve_turn_task}.

\subsection{I-AUV modeling in MoveIt!}\label{sec:model_moveit}
The available arm has a constrained workspace with a significant number of poses that would not be reachable by the arm without moving the vehicle. For this reason, the complete I-AUV was modelled as a \textbf{signle 8 DoFs} over-actuated system overcoming the shortage of the arm. This section will explain how the system (vehicle and arm) is modelled in MoveIt!, focusing on importing the system kinematic parameters into MoveIt! as explained in section \ref{modeling}

\begin{itemize}
\item \textbf{\textit{\underline{URDF:}}}\\
Fig. \ref{fig:arm_model} shows the I-AUV with the reference frame of each arm joint, along with the  arm Denavit-Hartenberg Kinematic Parameters table, while Fig. \ref{fig:moveit_arm_model} presents the tree structure of the system as modelled in MoveIt!. Similar to the example presented in Fig. \ref{fig:tree_sample} , the ellipses represent the links and the small cylinders the joints (with different types of joints having different colors as indicated in the legend).

\begin{figure}[ht!]
\centering
\includegraphics[width=0.5\linewidth]{figures/AUV_schema.png}
\begin{tabular}[b]{clllll}
\hline
\multicolumn{1}{|c|}{DOF} & \multicolumn{1}{l|}{$\theta$} & \multicolumn{1}{c|}{d}       & \multicolumn{1}{c|}{a}       & \multicolumn{1}{l|}{$\alpha$} & \multicolumn{1}{l|}{Home} \\ \hline
\multicolumn{1}{|c|}{1}   & \multicolumn{1}{l|}{$q_{1}$}  & \multicolumn{1}{c|}{0}       & \multicolumn{1}{l|}{$a_{1}=0.108$} & \multicolumn{1}{c|}{$\alpha1=-90$}      & \multicolumn{1}{c|}{0}    \\ \hline
\multicolumn{1}{|c|}{2}   & \multicolumn{1}{l|}{$q_{2}$}  & \multicolumn{1}{c|}{0}       & \multicolumn{1}{l|}{$a_{2}=0.231$} & \multicolumn{1}{c|}{0}        & \multicolumn{1}{c|}{0}    \\ \hline
\multicolumn{1}{|c|}{3}   & \multicolumn{1}{l|}{$q_{3}$}  & \multicolumn{1}{c|}{0}       & \multicolumn{1}{l|}{$a_{3}=0.1$} & \multicolumn{1}{c|}{$\alpha2=90$}       & \multicolumn{1}{c|}{0}    \\ \hline
\multicolumn{1}{|c|}{4}   & \multicolumn{1}{l|}{$q_{4}$}  & \multicolumn{1}{l|}{$d_{4}=0.308$} & \multicolumn{1}{c|}{0}       & \multicolumn{1}{c|}{0}        & \multicolumn{1}{c|}{0}    \\ \hline
\multicolumn{1}{l}{}      &                               &                              &                              &                               &                           \\
\multicolumn{1}{l}{}      &                               &                              &                              &                               &                           \\
\multicolumn{1}{l}{}      &                               &                              &                              &                               &                          
\end{tabular}
\caption{I-AUV Schema \& Arm Denavit-Hartenberg Kinematic Parameters}
\label{fig:arm_model}
\end{figure}

In Fig. \ref{fig:moveit_arm_model}, we can see first the reference frame called by default Rviz, since in the underwater domain the positive Z-axis points downwards, another frame has been defined \textbf{"world"} with a rotation of  $-\Pi$ around the X-axis to perform the required transformation. Thus, the first two links in the figure are virtual,  just for the sake of adjusting the reference frame for the whole model, and they are linked with a fixed joint.
\\
\textit{\textbf{Vehicle Modelling:}} \textbf{The AUV} has 6 DoFs, although the roll and pitch are passively stable, that is why the vehicle is actuated only in 4 DoFs. It is modelled as three consecutive prismatic joints representing $(X,Y,Z)$ displacements, followed by the three joints corresponding to $(\phi,\theta,\psi)$. The $\psi$ joint is modelled as continuous with no limits on the joint rotation, and the $(\phi,\theta)$ are modelled as fixed joints.
The three prismatic joint-limits are bounded by the size of the water tank where the experiments were held (details in section [\ref{sec:methodology}]). 


\begin{figure}[ht!]
\centering
\includegraphics[width=1.25\linewidth]{figures/IAUV_model.jpg}
\caption{The 8 DOF I-AUV as Modelled in MoveIt!}
\label{fig:moveit_arm_model}
\end{figure}


\textit{\textbf{Arm Modelling:}} As explained before, the system kinematic parameters are modelled under the \textbf{\textless joint\textgreater} tag. The mapping between the Denavit-Hartenberg parameters and URDF tags is listed in Table \ref{mapping} (with the designated tag in bold).
First, \textbf{the $\theta$} is the joint value for a non-fixed joint, it changes along the motion of the model and is not pre-defined in the URDF. The arm has four revolute joints with the corresponding $\theta$ values indicated as (Q=[q1 q2 q3 q4]).  Both \textbf{the d and the a} parameters affect the origin \textless x,y,z\textgreater tag, while \textbf{the $\alpha$} affect the origin\textless r,p,y\textgreater tag. As shown in Fig. \ref{fig:moveit_arm_model}, the arm links and joints were modelled using the mapping in Table \ref{mapping}. Each joint limit is specified with a small margin then the actual joint limit to ensure safety of the system.

\begin{table}[h]
\begin{tabular}{|c||c|}
\hline
Denavit-Hartenberg Parameters & URDF Tag Mapping                                                                                                                                                 \\ \hline
\textbf{$\theta$}           & \begin{tabular}[c]{@{}c@{}}For non-fixed joints: not pre-defined in the URDF,\\ change during the rotation/translation of the joint\end{tabular}             \\ \hline
\textbf{d}                  & Mapped to: \textless joint\textgreater\textless origin\textgreater\textbf{\textless x,y,z\textgreater}\textless origin/\textgreater\textless joint/\textgreater                      \\ \hline
\textbf{a}                  & Mapped to: \textless joint\textgreater\textless origin\textgreater\textbf{\textless x,y,z\textgreater}\textless origin/\textgreater\textless joint/\textgreater           \\ \hline
\textbf{$\alpha$}           & Mapped to: \textless joint\textgreater\textless origin\textgreater\textbf{\textless r,p,y\textgreater}\textless origin/\textgreater\textless joint/\textgreater           \\ \hline
\end{tabular}
\caption{Mapping Denavit-Hartenberg Parameters \& URDF tags}
\label{mapping}
\end{table}


\textit{\textbf{Vehicle-Arm Link Modelling:}} In Fig. \ref{fig:moveit_arm_model}, the fixed joint between the vehicle and the arm is virtual. It represents the base of the arm, and its purpose is to define the transformation between the vehicle centre and the arm base, thus linking both models together as one system. Adding this virtual joint allows keeping both models (i.e. the AUV and the arm) separated and puts aside the model specific details, which enables their reusability and simple reallocation of the arm (e.g. to place the arm at the bottom of the AUV for different kind of manipulation tasks).

For all revolute/continuous/prismatic  joints in the system, the \textbf{\textless axis\textgreater} tag defines the direction of the rotation/translation.  The visual and collision properties of the model are defined through  the \textless link\textgreater tags (as explained in Section \ref{modeling}).
The full URDF used to model \emph{Girona500} I-AUV is listed in Appendix \ref{ch:urdf_extra}.

\item \textbf{\textit{\underline{SRDF:}}}\\
The process of creating the SRDF explained in section [\ref{modeling}] was followed. The most important part was creating the \textbf{planning groups}. For the available system, two groups were defined:\\
Firs the \textbf{"Arm"} group where all the AUV and arm joints were added. This was the group used during all the planning and manipulation tasks. Another group, named \textbf{"Gripper"}, was also defined. Usually, this would be the group used for grasping tasks, but since only a passive fixed gripper has been used in the experiments, it was modelled for the sake of completeness and flexibility of replacement with other models, but it was not really used.
The full SRDF of the model is listed in Appendix \ref{ch:urdf_extra}.
\end{itemize}

The results of modelling \textbf{\emph{Girona500}} I-AUV in MoveIt! following the previous steps, as well as its collision model used for collision avoidance are shown in Fig.[\ref{fig:girona50_moveit_model}]

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.5\textwidth}
  \centering
  \includegraphics[width=0.65\linewidth]{figures/model.png}
  \caption{The 3D Visual Model}
  \label{fig:girona50_moveit_visual_model}
\end{subfigure}%
\begin{subfigure}[b]{.5\textwidth}
  \centering
  \includegraphics[width=0.65\linewidth]{figures/collision_model.png}
  \caption{The 3D Collision Model}
  \label{fig:girona50_moveit_collision_model}
\end{subfigure}
\caption{Girona500 Model In MoveIt!}
\label{fig:girona50_moveit_model}
\end{figure}

\subsection{I-AUV Kinematics Solver}
As explained in Section \ref{kdl}, the default kinematic solver (KDL), was used and configured through the setup assistant. Its parameters have been configured as follow: \\
$[kinematics\_solver\_search\_resolution:0.05,   kinematics\_solver\_timeout:5,\\ kinematics\_solver\_attempts:10]$. The time-out and the number of attempts were changed from the default values, increasing the probability of finding a kinematic solution for a given request.
Even the KDL kinematic solver is considered slow, in the context of this project, it did the job and no other solvers were investigated.

\subsection {I-AUV Robot State \& Joint State Synchronization} 
As mentioned in Section \ref{robot_joint}, a joint state synchronization node (shown in Fig. \ref{fig:arch}), was created to receive the update from the real system, and generate MoveIt! specific joint state message. A call back to this publisher in the main node \textbf{\emph{"PlanningManager"}}, receives the joint state update and simultaneously update the planning scene and its equivalent robot state. Also the robot state publisher publishes all the transformations from one link frame to the next one as defined in the URDF.
\\
Fig. \ref{fig:uwsim_modevit_tf} shows the arm TFs as published by MoveIt!. The name of each frame maps each link name, and each frame transformation is published with respect to the previous frame (i.e. previous link). 

\begin{figure}[ht!]
\centering
  \includegraphics[width=0.5\linewidth]{figures/moveit_tf.png}
 
\caption{The Arm Joints Transformations Published In MoveIt! \& Real System }
\label{fig:uwsim_modevit_tf}
\end{figure}

\subsection{I-AUV Controller} 
A plug-in mechanism is used to integrate the system controller with the planning capabilities.
The process explained in Section \ref{sec:moveit_controller} has been followed. The created plug-in is shown in Fig. \ref{fig:arch}, in this case it acts as an interface between MoveIt! and the existing I-AUV controllers (see Section \ref{sec:girona500_sw_overview}). It receives MoveIt! planning output messages \textbf{[moveit\_msg/RobotTrajectory]}, that contains non-parametrized position, velocity and acceleration points to be followed by the robot in order to reach the desired goal.
In the current case, only position information is used. First, we split between vehicle and arm joints, for each the corresponding custom ROS message is created as expected by \textbf{\emph{Girona500}} architecture. A custom \textbf{\textit{[cola2\_control/WorkdWayPointReq]}} is used to control the AUV. While for the arm, the custom \textbf{\textit{[cola2\_control/joystick\_jnt\_abs]}} message is used to control the absolute position of each arm joint.
Since a synchronization between the vehicle and arm motion is required, both type of messages were sent at the same frequency (10 Hz), which is the rate required by the vehicle in order to follow the requests.
Also, in order to ensure that the real system reaches the desired positions, a feed-back loop was essential. Fig. \ref{fig:moveit_auv_controller} shows both the feed-forward and feed-back loops with the corresponding messages in each direction.

\begin{figure}[ht!]
\centering
\includegraphics[width=0.85\linewidth]{figures/moveit_auv_controller.jpg}
\caption{MoveIt! System Controller \& Its Interface With I-AUV Controller}
\label{fig:moveit_auv_controller}
\end{figure}

Since we can not expect the accuracy of both the vehicle and the arm controllers to be perfect, a tolerance-based controller loop was implemented (as shown in Fig. \ref{fig:moveit_auv_controller}. For each point in the trajectory received by MoveIt!, the arm and the vehicle messages are sent at a rate of 0.1 sec, until the tolerance for all the joints is achieved, then the controller moves to the next point. This tolerance is configurable, and due to the difference of nature of vehicle and arm joints, different tolerances are specified for each one.
\subsection{I-AUV Planning Capabilities}
The planning technique used in the implemented system is based on the use of "Move\_group" (as detailed in Section \ref{move_plan}). Both joint-space and Cartesian-space planning were used based on the need. Planning a set of way points and pick functionality were also tested.
The default MoveIt! motion planner plug-in [OMPL], and its default planning algorithm \textbf{"RRT-Connect"} were used during all the planning tasks.

\subsection {Workspace Analysis} \label{sec:workspace}

Since the focus of the project is "mobile manipulation", a question that arose is how to choose the manipulation pose (position and orientation), and which poses are reachable by the existing system. These questions lead to the idea of researching about what is called \textbf{\textit{workspace analysis}}. 
Workspace analysis can be defined as the set of poses a manipulator is able to reach given the kinematic constraints of the system. 
Defining this workspace is an important design criterion, for example at a design stage would help to decide on how to place the arm on the vehicle in order to undertake certain categories of tasks.
As known in the literature \cite{Zacharias2007}, the theoretically possible workspace of the robot arm can be encapsulated by a cube with a side-length of the arm lengths centred at the robot arm base. The enveloping cube is then subdivided into equally sized smaller cubes (uniform sampling). This discretization takes into account the position and the orientation (or only one of them if the other is fixed).\\
After this sampling process, one or more criteria are used to filter the reachable poses. A common criterion is the \textbf{manipulability index}. This index is a measure that takes into account how close/far a solution is from a singularity configuration of the system. The research in this area has proposed more advanced criteria as in \cite{Berenson2007},\cite{Vahrenkamp2012},\cite{Zacharias2009}, incorporating a penalty for configurations close to joint limits, collision penalization, and so on. For simplicity, in the implemented system only the manipulability index and collision avoidance, were taken into account. The manipulability index is computed based on analysing the manipulability ellipsoid that is spanned by the eigen vectors of the Jacobian as shown below: 
\begin{equation}
   w=\sqrt{det(JJ^{T})}
  \end{equation}


\section{ "Valve Turning" Benchmark Task} \label{sec:valve_turn_task}

As mentioned before, the sea intervention tasks are quiet recent. In the literature only three projects were able to show valve turning intervention (two using docking to stabilize the AUV and the third using learning by demonstration). So, this scenario was chosen as it can be considered a benchmark for intervention tasks, and can validate the use of MoveIt! in the underwater domain.
\\
For this scenario to be developed, a sub-sea mock-up panel with valves was used (shown in Fig \ref{fig:panel_axis}). The panel has four valves (numbered in Fig \ref{fig:panel_axis}).

\begin{figure}[ht!]
\centering
\includegraphics[width=0.5\linewidth]{figures/panel_axis.png}
\caption{Panel mounted in the water tank with its frame of reference shown}
\label{fig:panel_axis}
\end{figure}

In the figure, the world reference frame (at the top left),as well as the frame of reference of the panel center are shown. The valves have the same frame of reference as the panel, and their positions are fixed with respect to it. Once the panel is detected, computing the pose of each valve is easy as it is a priori known. Also, the panel acts as a landmark for the localization of the AUV once the navigation starts (as explained in Section \ref{sec:girona500_sw_overview}).\\
Originally all the valves are in a vertical position and it is required to turn the chosen valve horizontally  (as shown in valve $\#2$ in Fig \ref{fig:panel_axis}), the valves can only be turned a maximum of $\frac{\Pi}{2}$, anti-clockwise from the vertical position and clockwise from the horizontal.

\subsection{\textbf{\emph{Valve Turning Phases:}}}

Fig. \ref {fig:all_phases} shows a sketchily representation of the scenario phases. It is mainly splitted into three essential steps plus an optional one (related to obstacle avoidance).\\ 
The valve turning itself includes two sub-steps \textit{("Approach" and "Grasp \& Turn")}. Also workspace analysis capability was incorporated in the choice of the manipulation task pose under different conditions, as will be explained, a detailed flow chart of the scenario is provided in Appendix \ref{ch:flow_chart}:\\

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{figures/detection_scene.jpg}
  \caption{Detection}
  \label{fig:all_det}
\end{subfigure}%
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=0.75\linewidth]{figures/inspection_scene.jpg}
  \caption{Inspection}
  \label{fig:all_insp}
\end{subfigure}
\begin{subfigure}[b]{0.33\textwidth}
  \centering
  \includegraphics[width=0.75\linewidth]{figures/grasppose_choice.jpg}
  \caption{Grasp Pose Choice}
  \label{fig:all_choice}
\end{subfigure}%

\begin{subfigure}[b]{0.5\textwidth}
  \centering
  \includegraphics[width=0.55\linewidth]{figures/approach_scene.jpg}
  \caption{Approaching}
  \label{fig:all_approach}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
  \centering
  \includegraphics[width=1.1\linewidth]{figures/grasp_scene.jpg}
  \caption{Grasping \& Turning}
  \label{fig:all_grasp}
\end{subfigure}%
\caption{Valve Turning Phases}
\label{fig:all_phases}
\end{figure}


\begin{itemize}
\item \textbf{Detection:}\\
\textit{The purpose} of this step is to search for the panel. Once it is located, first the AUV is able to localize itself (using EKF-SLAM). Also, the valve pose with respect to the panel pose can be computed. Thus, allowing to deduce the valve pose with respect to the world reference.
Fig. \ref {fig:all_det} shows the environment and the detection points in red arrows. The base of each arrow represents the position while its direction shows the orientation of the point.

\begin{algorithm}[h]
\caption{Detection Flow}\label{det_flow}
\begin{algorithmic}[1]
\Procedure{DetectPanel (\textit{robot\_model,config})}{}
\State $\textit{planningScene.model} \gets \textit{robot\_model}$
\State $\textit{planningScene.World.Collision} \gets \textit{"PilarObstacle"}$
\State $\textit{updatePlanningScene}$
\State $\textit{planningGroup} \gets \textit{createMoveGroup("Arm")}$
\State $\textit{N} \gets \textit{config.NumOfDetectionPoints}$
\For{each index $i < N$ }
\State $\textbf{\textit{planningGroup.setJointTarget(detectionPose.x
,detectionPose.y}}$
\State $\textbf{\textit{                                                                           ,detectionPose.z,detectionPose.yaw)}}$
\If {$callROSService[isPanelDetected]$} 
\State $call \textit{updateWorldScene(panelPose)}$
\Return true
\EndIf
\State $Print("Detection\, \, points\, \, finished,\, \, panel\, \, not\, \, detected,\, \, stop!")$
\Return false
\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}

These points are configurable. The AUV is asked to navigate through them trying to locate the panel. The points are defined in the joint space using $(X,Y,Z,\psi)$  points for the AUV. The arm is kept in fixed pose. The step is terminated when the panel is detected.\\
The order and the place of these points are random and their change does not affect the flow of the scenario. If the panel is not detected after moving to the last point, the scenario stops.
The \textbf{\emph{PanelDetection}} node shown in \ref{fig:arch} is the one responsible of handling this process, and Algorithm \ref{det_flow} explains the technical details of this step.

\item \textbf{Inspection:}\\
\textit{Its purpose} is to ensure that the position of the AUV is close and oriented to the panel. This helps improving the estimation of the panel and the valve poses, and increases the accuracy of performing the valve turning.\\
Since the detection points are random, the panel can be detected anywhere moving from one detection point to the other. This does not ensure that the final pose of the AUV is good enough for accurate estimation of the panel and the valve poses.
The details of this step are explained in Algorithm \ref{inspect_flow}.

\begin{algorithm}[h]
\caption{Inspection Flow}\label{inspect_flow}
\begin{algorithmic}[1]
\Procedure{InspectPanel ()}{}
\State $\textit{inspectionPose[]} \gets \textit{callROSService[getInspectionPoseInWorldFrame]}$
\State $\textit{planningGroup} \gets \textit{createMoveGroup("Arm")}$
\State $\textit{N} \gets \textit{NumOfInspectionPoints}$
\For{each index $i < N$ }
\State $\textit{inspectPose} \gets \textit{point[index]}$
\State $\#The\, \, pose\, \, here\, \, includes\, \, position\, \, and\, \, orientation$
\State $\textbf{\textit{planningGroup.setPoseTarget(inspectPose)}}$
\If {$planning succeed$} 
\Return true
\EndIf
\State $Print("Moving\, \, to\, \, Inspection\, \, points\, \, done\, \, with\, \, failure,\, \, stop!")$
\Return false
\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}

The inspection points are defined as End-Effector poses (points in the Cartesian space), and  computed at configurable distance from the panel, with different orientations.
Since they are defined in the panel frame of reference,  a transformation from the panel frame to the world frame has to be applied. \\
Fig. \ref{fig:all_insp} shows the inspection points. The base of each arrow represents the position while its direction shows its orientation.

\item \textbf{Grasp Pose Choice:}\label{choice_grasp}\\ 
In order to perform a manipulation task, we have to choose the Pose (position \& orientation) in which the task can be accomplished. In the task at hand, the position is defined and can be computed with respect to the panel (at a small fixed distance from the estimated valve position). Also the ideal orientation would be the one orthogonal to the valve, giving access to the V-shape gripper to turn the valve. 
Since the X-axis of the End-Effector reference frame aligns itself with the direction of a given orientation, the orthogonal orientation to the panel was computed  by applying $-\pi/2$ rotation around the panel Z-axis, followed by $-\pi$ rotation around its X-axis.
Fig. \ref{fig:all_choice} shows both the valve frame of reference on the top left, and the orthogonal orientation computed.\\
Finally the grasp pose is composed of the valve position and the orthogonal orientation as in the figure.
\clearpage

\item \textbf{Valve Turning:}\\
\textit{The purpose} of this step is to explain how the manipulation task is performed. As mentioned before, it implies two sub-steps: \textit{("Approach" and "Grasp \& Turn")}. The approach step has the goal of getting close to the grasp pose at a pre-defined distance and with the same orientation as the grasp (i.e. orthogonal to the panel). It guarantees the posture of the end-effector before grasping.
Both the approach and grasp poses are shown in Fig. \ref{fig:all_approach} and Fig. \ref{fig:all_grasp}.

\begin{algorithm}[h]
\caption{Plan \& Turn Flow}\label{plan_flow}
\begin{algorithmic}[1]
\Procedure{PlanAndTurnValve \textit{(config)}}{}
\State $\textit{wayPointsPose[]} \gets \textit{callROSService[getWayPointsPoseInWorldFrame]}$
\State $\textit{planningGroup} \gets \textit{createMoveGroup("Arm")}$
\State $\textit{N} \gets \textit{NumOfWayPoints}$
\For{each index $i < N$ }
\State $\textit{wayPointPose} \gets \textit{point[index]}$

\If {$lastPoint$} 
	\If {$generateGrasps$} 
		\State $\textit{bestGraspPose} \gets \textit{call:Generate\_Filter\_Grasp(options,wayPointsPose[i]) }$
	\Else
		\State $\textit{bestGraspPose} \gets  \textit{wayPointsPose[i]) }$
	\EndIf	
\State $\textit{wayPointPose} \gets \textit{bestGraspPose}$
\EndIf
\State $\#The\, \, pose\, \, here\, \, includes\, \, position\, \, and\, \, orientation$
\State $\textbf{\textit{planningGroup.setPoseTarget(wayPointPose)}}$
\If {$(planning succeed \&\& lastPoint)$} 
\State $\textit{callROSService[turnValveDesiredDegree]}$
\Return true
\EndIf
\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}

Two different MoveIt! features were used to implement this step in different ways. \\
\textbf{First grasping method} uses path planning through several consecutive way-points, where the first way point acts as the approach step, and the last way-point constitutes the grasp operation.
In this technique, MoveIt! \textbf{\emph{"plan" \& "execute"}} methods are used for each way-point separately. When the last point is reached, a service provided by the arm controller is called to turn the wrist joint the required degrees to complete the action (see Algorithm \ref{plan_flow}).\\ 
An alternative to the separate MoveIt! planning calls, is to plan through a set of way points at once by calling \textbf{\emph{computeCartesianPath}} method. This method generates hundreds of points with small steps to ensure a good trajectory tracking. Due to problems with the AUV controller to follow way-points with such precision, the execution time was higher and sometimes even it fails to reach this precision, so this method was avoided in the experimental tests.

\begin{algorithm}[h]
\caption{Pick \& Turn Flow}\label{pick_flow}
\begin{algorithmic}[1]
\Procedure{PickValve \textit{(config)}}{}
\State $\textit{graspPose} \gets \textit{callROSService[getGraspPoseInWorldFrame]}$
\State $\textit{planningGroup} \gets \textit{createMoveGroup("Arm")}$
\State $\textit{grasp\_msg} \gets \textit{createGraspMsg}$
\State $\#Fill Grasp message parameters$
\State $\textit{grasp\_msg.approach.frame} \gets \textit{"graspObject"}$
\State $\textit{grasp\_msg.approach.direction} \gets \textit{z}$
\State $\textit{grasp\_msg.approach.distance} \gets \textit{0.3}$
\State $\textit{grasp\_msg.grasp\_posture.joint} \gets \textit{"Wrist Joint"}$
\State $\textit{grasp\_msg.grasp\_posture.position} \gets \textit{config.ValveTurningDegree}$
\If {$generateGrasps$} 
	\State $\textit{bestGraspPose} \gets  \textit{call: Generate\_Filter\_Grasp(options,grasp\_msg) }$
\Else
\State $\textit{bestGraspPose} \gets  \textit{graspPose) }$
\EndIf
\State $\textit{grasp\_msg.grasp\_pose} \gets \textit{bestGraspPose}$
\State $\textbf{\textit{planningGroup.pick("graspObject",grasp\_msg)}}$
\EndProcedure
\end{algorithmic}
\end{algorithm}

\textbf{The second technique} exploits MoveIt! pick pipeline explained in Section \ref{pickfun}.
The nature of the manipulation task at hand is different from common ones, as it does not involve picking an object and changing its place. But, the goal was to adjust the pick pipeline (shown in Fig. \ref{fig:grasp_all}), to fit the valve turning scenario needs as follow:
\begin {itemize}
\item The approach way-point in the first technique, was replaced by the approach direction specified in MoveIt! grasp message.
\item The grasp pose is the valve pose previously computed as explained above.
\item  The grasp posture usually used to identify the closure of the gripper after picking the object, was here used to indirectly pass to the controller the desired turning degree of the wrist joint.
\end{itemize}
In this scenario, both the retreat and the pre-grasp features were not needed. A detailed pseudo-code is shown in Algorithm \ref{pick_flow}.

\item \textbf{Obstacles Management:}\\
As mentioned at the beginning, this step is optional and can be placed anywhere in the flow. The goal is to use MoveIt! obstacle avoidance capability and implement obstacle avoidance during the intervention as detailed in Algorithm \ref{obstacle_flow}. Two types of obstacles were added as shown in Fig. \ref{fig:obstacles_scene}, where the experimental environment is represented.
 
\begin{figure}[ht!]
\centering
 \includegraphics[width=0.55\linewidth]{figures/obstacles_scene.jpg}
  \caption{Obstacles Scene In Valve Turning Scenario}
\label{fig:obstacles_scene}
\end{figure}

\begin{algorithm}[h]
\caption{Obstacles Management}\label{obstacle_flow}
\begin{algorithmic}[1]
\Procedure{updateWorldScene (\textit{panelPose})}{}
\State $\textit{planningScene.World.Collision} \gets \textit{"PanelObject"}$
\State $update \textit{AllowanceCollisionMatrix}$
\State $computeWallPoseInWorldFrame$
\State $\textit{planningScene.World.Collision} \gets \textit{"WallBehindPanel"}$
\State $update \textit{AllowanceCollisionMatrix}$
\State $\textit{valvesPose[]} \gets \textit{callROSService[getValvesPoseInWorldFrame]}$
\For{each index $i < 3$ } $\#Add three valves only as obstacles$
\State $\textit{planningScene.World.Collision} \gets \textit{"valve[i]"}$
\State $update \textit{AllowanceCollisionMatrix}$
\EndFor
\State $\# Adding\, \, this\, \, obstacle\, \, is\, \, optional\, \, as\, \, it\, \, makes\, \, turning\, \, the\, \, valve\, \, harder$
\State $computeObstaclePoseInWorldFrame$
\State $\textit{planningScene.World.Collision} \gets \textit{"wallObstacle"}$
\State $update \textit{AllowanceCollisionMatrix}$
\EndProcedure
\end{algorithmic}
\end{algorithm}

\begin{itemize}
\item Objects that exist in reality which have to be avoided during navigation and manipulation: the  panel and the valves, as well as the wall behind the panel to ensure the safety of the I-AUV.
\item The second type of obstacles are virtual objects. They do not exist in the environment and are simulated in order to see the changes in the planning behaviour and test more complex scenarios. The "detection obstacle" pillar (shown in Fig. \ref{fig:obstacles_scene}) was used to see how the generated path between detection points changes. The other one ("Panel Obstacle" in the figure), was added close to the panel to force changing the posture of the valve turning and showing the benefit of the workspace analysis incorporated in the manipulation process (as will be shown in the next section). 

\end{itemize}

\item \textbf{Workspace Analysis:}\\
An important consideration in the system at hand, is that both the AUV and the arm were modelled as one system, thus the workspace analysis is done for the whole system not only the arm.\\
As explained before, the grasp pose is fixed. But what if the environment changes or the manipulation location became tight due to obstacles (as indicated in Fig. \ref{fig:obstacles_scene} ). As a result, the planning can fail to find a collision-free path to reach the grasp pose with the specified orthogonal orientation.
Here comes the importance of the workspace analysis in order to choose a possible and valid grasping configuration. Only orientation analysis is performed as the valve position is fixed. In order to analyse the possible orientations, a discretization of  $(\phi,\theta,\psi)$ at the valve position was performed in a half circle as shown in Fig. \ref{fig:workspace_all}.
It shows a side view of the half-circle of the space analysed (and on the top right a zoomed front view). All the arrows in red indicate invalid configurations. The nature of the valve turning scenario adds an extra constraint on the valid orientations, as the gripper V-shape has to fit the valve T-shape to be able to perform the turn. So, ideally only orthogonal orientations (positive and negative) are the best option. But due to the size of the gripper and the space available between the valve and the gripper, a tolerance with some degrees on each side will still do the job. So, a filtering specific to the valve turning task is needed to chose only orientations in the plan of the panel with the mentioned tolerance degrees. The result of this filtering is shown in Fig. \ref{fig:workspace_ok}.

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.5\textwidth}
  \centering
  \includegraphics[width=0.55\linewidth]{figures/workspace_all.png}
  \caption{Sampled Workspace}
  \label{fig:workspace_all}
\end{subfigure}%
\begin{subfigure}[b]{.5\textwidth}
  \centering
  \includegraphics[width=0.55\linewidth]{figures/good_conf_valve.png}
  \caption{Workspace Valid For Valve Turning}
  \label{fig:workspace_ok}
\end{subfigure}

\begin{subfigure}[b]{.5\textwidth}
  \centering
  \includegraphics[width=0.55\linewidth]{figures/good_conf_filtered.png}
  \caption{Filtered Workspace With "Detection Obstacle"}
  \label{fig:workspace_filtered}
\end{subfigure}%
\begin{subfigure}[b]{.5\textwidth}
  \centering
  \includegraphics[width=0.55\linewidth]{figures/good_conf_obstacle.png}
  \caption{Filtered Workspace With "Panel Obstacle"}
  \label{fig:workspace_obstacle}
\end{subfigure}
\caption{Workspace Analysis Phases}
\label{fig:workspace_all_filtered}
\end{figure}


\begin{algorithm}[h]
\caption{Generate \& Filter Grasp Pose}\label{filter_flow}
\begin{algorithmic}[1]
\Procedure{Generate\_Filter\_Grasp (generateOptions,actualGraspPose)}{}
\For{(each index $i\, in\, generateOptions)$ }
\State $\textit{newGraspPose.position} \gets \textit{actualGraspPose.position}$
\State $\textbf{\textit{possible\_grasps.add(newGraspPose)}}$
\EndFor
\For{(each index $i\, in\,possible_grasps)$ }
\State $\textit{isValidGrasp} \gets \textit{ik\_service.solve(possible\_grasps[i])}$
\If {$isValidGrasp$} 
	\State $\textit{possible\_grasps[i].quality} \gets \textit{computeManipulabilityIndex}$
	\State $\textbf{\textit{filtered\_grasps.add(possible\_grasps[i])}}$
\EndIf
\EndFor
\State $\textbf{\textit{orderByQuality(filtered\_grasps)}}$
\EndProcedure
\end{algorithmic}
\end{algorithm}

The next filtering step is based on the kinematically reachable poses and obstacle avoidance.
Only reachable poses leading to collision-free grasping are selected. Fig.\ref{fig:workspace_filtered} shows the final analysis result in case of "Detection Obstacle" only, while Fig. \ref{fig:workspace_all_filtered} shows the result in case of the environment modelled as in Fig. \ref{fig:obstacles_scene} , with "Panel Obstacle" added.
 The different colors of the arrows indicate their manipulability index (i.e. grasp quality), the blue has higher quality than the yellow and green.  It is clear that around $20\%$ or less (for more complex environment) are valid. \\
This analysis was useful to verify the decision previously made about the grasp orientation. Also, it added more flexibility to the orientation choice, given complicated environments.
The \textbf{\emph{Grasp Analysis}} node shown in \ref{fig:arch} is responsible of this functionality, where its technical details are presented in Algorithm \ref{filter_flow}, 

In Fig. \ref{fig:workspace_good_config}, samples of the valid poses are shown, along with the corresponding system configuration. While, Fig. \ref{fig:workspace_bad_all} shows samples of the invalid poses, mostly due to inconvenience with the valve turning task or due to collision with the panel. 

\begin{figure}[ht!]
\centering
 \begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/good_2.png}
  \label{fig:bad_all}
\end{subfigure}%
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/good_1.png}
  \label{fig:ee_xyz_2}
\end{subfigure}
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/good_3.png}
  \label{fig:ee_rpy_2}
\end{subfigure}%
\caption{Sample of Valid Orientations With I-AUV Configuration At Each Sample}
\label{fig:workspace_good_config}
\end{figure}

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/bad_all.png}
  \label{fig:bad_all}
\end{subfigure}%
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/bad_1.png}
  \label{fig:ee_xyz_2}
\end{subfigure}
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{figures/bad_2.png}
  \label{fig:ee_rpy_2}
\end{subfigure}%
\caption{Sample of Invalid Orientations With I-AUV Configuration At Each Sample}
\label{fig:workspace_bad_all}
\end{figure}


\end{itemize}






